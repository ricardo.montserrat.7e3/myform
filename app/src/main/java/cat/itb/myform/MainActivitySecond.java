package cat.itb.myform;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

public class MainActivitySecond extends AppCompatActivity
{
    public static final byte HOLA = 1;
    public static final byte ADEU = 0;

    RadioButton radioHola;
    RadioButton radioAdeu;

    SeekBar ageBar;
    TextView ageText;

    Button nextStepButton;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_second);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        radioHola = findViewById(R.id.radioHola);
        radioAdeu = findViewById(R.id.radioAdeu);

        ageBar = findViewById(R.id.ageSeekBar);
        ageText = findViewById(R.id.ageText);

        nextStepButton = findViewById(R.id.nextStepButton);

        ageBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { ageText.setText(String.valueOf(progress)); }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                if (ageBar.getProgress() < 18 || ageBar.getProgress() > 60)
                {
                    if(nextStepButton.getVisibility() == View.VISIBLE) nextStepButton.setVisibility(View.INVISIBLE);
                    Toast.makeText(MainActivitySecond.this, "Your age can only be between 18 and 60", Toast.LENGTH_SHORT).show();
                }
                else if (nextStepButton.getVisibility() == View.INVISIBLE) { nextStepButton.setVisibility(View.VISIBLE); }
            }
        });

        findViewById(R.id.nextStepButton).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (radioHola.isChecked() || radioAdeu.isChecked())
                {
                    Bundle bundle = getIntent().getExtras();

                    if (bundle != null)
                    {
                        Intent intent = new Intent(MainActivitySecond.this, MainActivityThird.class);
                        intent.putExtra("nom", bundle.getString("nom"));
                        intent.putExtra("checked", radioHola.isChecked() ? HOLA : ADEU);
                        intent.putExtra("age", (byte) ageBar.getProgress());
                        startActivity(intent);
                    }
                    else
                        Log.d("Main activity second", "nextStepButton onClick: no extras in bundle");
                }
                else
                    Toast.makeText(MainActivitySecond.this, "Please, check an option first", Toast.LENGTH_SHORT).show();
            }
        });
    }
}