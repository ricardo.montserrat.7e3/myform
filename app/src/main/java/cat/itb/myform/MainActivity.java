package cat.itb.myform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private EditText editTextNom;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        editTextNom = findViewById(R.id.nomEditText);

        findViewById(R.id.nextStepButton).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String nom = editTextNom.getText().toString();
                if (!nom.isEmpty())
                {
                    Intent intent = new Intent(MainActivity.this, MainActivitySecond.class);
                    intent.putExtra("nom", nom);
                    startActivity(intent);
                }
                else
                    Toast.makeText(MainActivity.this, "Please, enter a name first!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}