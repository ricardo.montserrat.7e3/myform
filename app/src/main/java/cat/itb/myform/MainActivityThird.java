package cat.itb.myform;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Objects;

public class MainActivityThird extends AppCompatActivity
{
    String message;

    private void setMessage()
    {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            message = bundle.getByte("checked") == MainActivitySecond.HOLA ? "Hola " + bundle.getString("nom") + ", com portes aquests " + bundle.getByte("age") + " anys?" : "Espero tornar a veure’t " + bundle.getString("nom") + ", abans que facis " + (bundle.getByte("age")+1) + " anys";
        }
        else Log.d("Main activity third", "buttonShow onClick: no extras in bundle");
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_third);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        setMessage();

        findViewById(R.id.buttonShow).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(MainActivityThird.this, message, Toast.LENGTH_SHORT).show();
                ((Button) v).setVisibility(View.INVISIBLE);
            }
        });

        findViewById(R.id.shareButton).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);

                startActivity(shareIntent);
            }
        });
    }
}